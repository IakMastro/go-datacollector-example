package main

import (
	"fmt"
	"log"
	"os"
	"net/http"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"github.com/go-redis/redis/v8"
	"github.com/joho/godot-env"
	"github.com/gin-gonic/gin"
	socketio "github.com/googolee/go-socket.io"
	"github.com/googolee/go-socket.io/engine"
	"github.com/googolee/go-socket.io/engine/transport"
	"github.com/googolee/go-socket.io/engine/transport/polling"
	"github.com/googolee/go-socket.io/engine/transport/websocket"

)
/*
TODO:
1. Start rewrite from Swarmlab project
2. Check if it works with the rest of the system
*/
func GinMiddleware(allowOrigin string) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", allowOrigin)
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With, device-remember-token, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Origin, Accept")

		if c.Request.Method == http.MethodOptions {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}

		c.Request.Header.Del("Origin")
		c.Next()
	}
}

func main() {
	err := godotenv.Load()
	
	if err != nil {
		log.Fatal("error loading .env file")
	}

	router := gin.New()

	server := socketio.NewServer(nil)

	ctx, cancel := context.WithTimeout(context.Background(), 2 * time.Second)
	mongodb, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://%s/", os.Getenv("MONGO_URL")))
	go func() {
		if err != nil {
			log.Fatlf("Mongodb listen error: %s\n", err)
		}
	}()
	defer cancel()

	redis_client := redis.NewClient(&redis.Options{
		Addr: os.Getenv("REDIS") + ":" + os.Getenv("REDIS_PORT"),
		Password: "",
		DB: 0
	}

	go func() {
		if err := server.Serve(); err != nil {
			log.Fatlf("socketio listen error: %s\n", err)
		}
	}()
	defer server.Close()

	router.Use(GinMiddleware("http://localhost:" + os.Getenv("PORT")) 
	router.GET("/socket.io/*any", gin.WrapH(server))
	router.POST("/socket.io/*any", gin.WrapH(server))

	if err := router.Run(":8000"); err != nil {
		log.Fatal("failed run app: ", err)
	}
}
