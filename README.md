# Golang Datacollector Example

This repo is made for Swarmlab Hybrid. It's a rewrite of [this nodejs application](https://git.swarmlab.io:3000/swarmlab/poc-datacollector/src/branch/master/readmongo).
Basically it's a middleware that opens two sockets between a client and a server.

# Requirements

- Docker

