FROM golang:alpine

WORKDIR /go/src/app

COPY . .

RUN go get .

EXPOSE 3000

CMD ["go", "run", "."]
